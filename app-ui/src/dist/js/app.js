const weatherForm = document.querySelector('form');
const search  = document.querySelector('input');
const errorMessage = document.querySelector('.error');
const weatherMessage = document.querySelector('.weather');


weatherForm.addEventListener('submit', (e) => {
    e.preventDefault();
    errorMessage.textContent = '';
    weatherMessage.textContent = '';
    const location = search.value;

    fetch(`/weather?address=${location}`).then((res) => {
        
        res.json().then((data) => {
            if(data.error){
                errorMessage.textContent = data.error;
            } else {
                weatherMessage.textContent = `${data.location}, ${data.forecast}`
            }
        })
    });
})