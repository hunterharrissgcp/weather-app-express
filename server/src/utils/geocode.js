const request = require('request');

const geocode = (address, callback) => {
    const url_mapBox = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(address)}.json?access_token=`;
    request({ url: url_mapBox, json: true }, (error, {body}) => {
        if (error) {
            if(callback){
                callback('Unable to connect to location services', undefined);
            }
            
        } else if (body.features.length === 0) {
            if(callback){
                callback('Unable to find location', undefined)
            }
            
        } else {
            const data = {
                latitude: body.features[0].center[1],
                longitude: body.features[0].center[0],
                location: body.features[0].place_name
            };
            if(callback){
                callback(undefined, data);
            }
        
        }
    });
};

module.exports = geocode;