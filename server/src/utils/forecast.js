const request = require('request');

const access_key = '';

const forecast = (lat, long, callback) => {
    const url_weatherStack = `http://api.weatherstack.com/current?access_key=${access_key}&query=${lat},${long}&units=f`;
    request({ url: url_weatherStack, json: true }, (error, { body }) => {
        if (error) {
            if (callback) {
                callback('Cannot connect to weather service', undefined);
            }

        } else if (body.error) {
            if (callback) {
                callback('Cannot find location', undefined);
            }

        } else {
            const data = `${body.current.weather_descriptions[0]}. It is currently ${body.current.temperature} degress out.`
            if (callback) {
                callback(undefined, data);
            }
        }
    });
};

module.exports = forecast;
