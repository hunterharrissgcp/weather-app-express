
const path = require('path');
const express = require('express');
const hbs = require('hbs');
const geoCode = require('./utils/geocode');
const forecast = require('./utils/forecast');

const app = express();
const port = 3000;

// Define Paths
const directoryPath = path.join(__dirname, '../../app-ui/src/dist');
const viewsPath = path.join(__dirname, '../templates/views');
const partialPath = path.join(__dirname, '../templates/partial');

// Setup handlebars
app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialPath);

// Set up static directory to serve
app.use(express.static(directoryPath));

// Index page
app.get('', (req, res) => {
    res.render('index', {
        title: 'Weather',
        name: 'other turkeys'
    });
});

// Help page
app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help',
        help: 'Help Page YEAH'
    });
});

// About page
app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About',
        about: 'about Page YEAH'
    });
})

// Weather page
app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'Please provide an address'
        })
    }

    geoCode(req.query.address, (error, { latitude, longitude, location }=  {}) => {
        if (error) {
            return res.send({ error })
        }
        forecast(latitude, longitude, (forecastError, forecastData) => {
            if (forecastError) {
                return res.send({ error });
            }
            res.send({
                forecast: forecastData,
                location,
                address: req.query.address
            });
        })
    })
    // res.send({
    //     hello: 'hello',
    //     bye: 'bye',
    //     address: req.query.address
    // });
});

app.get('/help/*', (req, res) => {
    res.render('404', {
        title: '404',
        message: 'NOT FOUND'
    });
})

app.get('*', (req, res) => {
    res.render('404', {
        title: '404',
        message: 'NOT FOUND'
    });
});

app.listen(port, () => {
    console.log(`Running on port ${port}`);
});